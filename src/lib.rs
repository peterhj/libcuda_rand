extern crate cuda;
extern crate libc;

use ffi::*;

use cuda::runtime::{CudaStream};

use libc::{c_ulonglong, size_t};
use std::ptr::{null_mut};

pub mod ffi;

pub type CurandResult<T> = Result<T, curandStatus_t>;

pub struct CurandGenerator {
  inner: curandGenerator_t,
}

impl Drop for CurandGenerator {
  fn drop(&mut self) {
    match unsafe { curandDestroyGenerator(self.inner) } {
      curandStatus_t::CURAND_STATUS_SUCCESS => {}
      e => panic!("PANIC: failed to destroy curand generator: {:?}", e),
    }
  }
}

impl CurandGenerator {
  pub fn create() -> CurandResult<CurandGenerator> {
    let mut gen: curandGenerator_t = null_mut();
    match unsafe { curandCreateGenerator(
        &mut gen as *mut curandGenerator_t,
        curandRngType_t::CURAND_RNG_PSEUDO_DEFAULT,
    ) } {
      curandStatus_t::CURAND_STATUS_SUCCESS => Ok(CurandGenerator{inner: gen}),
      e => Err(e),
    }
  }

  pub fn set_stream(&self, stream: &CudaStream) -> CurandResult<()> {
    match unsafe { curandSetStream(self.inner, stream.ptr) } {
      curandStatus_t::CURAND_STATUS_SUCCESS => Ok(()),
      e => Err(e),
    }
  }

  pub fn set_seed(&self, seed: u64) -> CurandResult<()> {
    match unsafe { curandSetPseudoRandomGeneratorSeed(self.inner, seed as c_ulonglong) } {
      curandStatus_t::CURAND_STATUS_SUCCESS => Ok(()),
      e => Err(e),
    }
  }

  pub fn set_offset(&self, offset: u64) -> CurandResult<()> {
    match unsafe { curandSetGeneratorOffset(self.inner, offset as c_ulonglong) } {
      curandStatus_t::CURAND_STATUS_SUCCESS => Ok(()),
      e => Err(e),
    }
  }

  pub unsafe fn generate_normal(&self, dst: *mut f32, length: usize, mean: f32, std: f32) -> CurandResult<()> {
    match curandGenerateNormal(self.inner, dst, length as size_t, mean, std) {
      curandStatus_t::CURAND_STATUS_SUCCESS => Ok(()),
      e => Err(e),
    }
  }

  pub unsafe fn generate_uniform(&self, dst: *mut f32, length: usize) -> CurandResult<()> {
    match curandGenerateUniform(self.inner, dst, length as size_t) {
      curandStatus_t::CURAND_STATUS_SUCCESS => Ok(()),
      e => Err(e),
    }
  }
}
