use cuda::ffi::runtime::{cudaStream_t};
use libc::{c_int, c_float, c_ulonglong, size_t};

enum curandGenerator_st {}
pub type curandGenerator_t = *mut curandGenerator_st;

#[derive(Debug)]
#[repr(C)]
pub enum curandStatus_t {
  CURAND_STATUS_SUCCESS                   = 0,
  CURAND_STATUS_VERSION_MISMATCH          = 100,
  CURAND_STATUS_NOT_INITIALIZED           = 101,
  CURAND_STATUS_ALLOCATION_FAILED         = 102,
  CURAND_STATUS_TYPE_ERROR                = 103,
  CURAND_STATUS_OUT_OF_RANGE              = 104,
  CURAND_STATUS_LENGTH_NOT_MULTIPLE       = 105,
  CURAND_STATUS_DOUBLE_PRECISION_REQUIRED = 106,
  CURAND_STATUS_LAUNCH_FAILURE            = 201,
  CURAND_STATUS_PREEXISTING_FAILURE       = 202,
  CURAND_STATUS_INITIALIZATION_FAILED     = 203,
  CURAND_STATUS_ARCH_MISMATCH             = 204,
  CURAND_STATUS_INTERNAL_ERROR            = 999,
}

#[repr(C)]
pub enum curandOrdering_t {
  CURAND_ORDERING_PSEUDO_BEST     = 100,
  CURAND_ORDERING_PSEUDO_DEFAULT  = 101,
  CURAND_ORDERING_PSEUDO_SEEDED   = 102,
  CURAND_ORDERING_QUASI_DEFAULT   = 201,
}

#[repr(C)]
pub enum curandRngType_t {
  CURAND_RNG_TEST                     = 0,
  CURAND_RNG_PSEUDO_DEFAULT           = 100,
  CURAND_RNG_PSEUDO_XORWOW            = 101,
  CURAND_RNG_PSEUDO_MRG32K3A          = 121,
  CURAND_RNG_PSEUDO_MTGP32            = 141,
  CURAND_RNG_PSEUDO_MT19937           = 142,
  CURAND_RNG_PSEUDO_PHILOX4_32_10     = 161,
  CURAND_RNG_QUASI_DEFAULT            = 200,
  CURAND_RNG_QUASI_SOBOL32            = 201,
  CURAND_RNG_QUASI_SCRAMBLED_SOBOL32  = 202,
  CURAND_RNG_QUASI_SOBOL64            = 203,
  CURAND_RNG_QUASI_SCRAMBLED_SOBOL64  = 204,
}

#[link(name = "curand", kind = "dylib")]
extern "C" {
  pub fn curandCreateGenerator(generator: *mut curandGenerator_t, rng_ty: curandRngType_t) -> curandStatus_t;
  pub fn curandDestroyGenerator(generator: curandGenerator_t) -> curandStatus_t;
  pub fn curandSetGeneratorOffset(generator: curandGenerator_t, offset: c_ulonglong) -> curandStatus_t;
  pub fn curandSetGeneratorOrdering(generator: curandGenerator_t, order: curandOrdering_t) -> curandStatus_t;
  pub fn curandSetPseudoRandomGeneratorSeed(generator: curandGenerator_t, seed: c_ulonglong) -> curandStatus_t;
  pub fn curandSetStream(generator: curandGenerator_t, stream: cudaStream_t) -> curandStatus_t;
  pub fn curandGenerate(generator: curandGenerator_t, output_ptr: *mut c_int, num: size_t) -> curandStatus_t;
  pub fn curandGenerateNormal(generator: curandGenerator_t, output_ptr: *mut c_float, num: size_t, mean: c_float, stddev: c_float) -> curandStatus_t;
  pub fn curandGenerateUniform(generator: curandGenerator_t, output_ptr: *mut c_float, num: size_t) -> curandStatus_t;
}
